// File: full_resolver.h
// License: CC-BY-SA 4.0
// Author: bgstack15
// Startdate: 2020-09-02 13:59
// Title: Header for full_resolver
// Purpose: library for full_resolver
// History:
// Usage:
// References:
//    [1] https://stackoverflow.com/a/2693826/3569534
//    [2] strtok safety https://stackoverflow.com/a/23961526/3569534
//    [3] https://stackoverflow.com/questions/32845445/convert-ip-address-in-sockaddr-to-uint32-t/32845610#32845610
//		[4] https://codegolf.stackexchange.com/questions/197879/convert-a-32-bit-binary-ipv4-address-to-its-quad-dotted-notation/197925#197925
// Improve:
// Dependencies:
// Documentation:

#ifndef FULL_RESOLVER_H
#define FULL_RESOLVER_H 20200902
#include <stdio.h>
// If switch to getaddrinfo, need sys/types.h sys/socket.h netdb.h
#include <string.h>
#include <udns.h>
#include <linux/in.h> // proper form is to use arpa/inet.h but it will fail the implicit declaration of function ntohl

// Forward declarations
u_int32_t ntohl(struct in_addr); // have to declare this to avoid warning

int full_resolve(char*, struct dns_ctx* ctx);

int conv_ipv4_dec_to_dotquad(long ip);

// Functions
int full_resolve(char* name, struct dns_ctx *ctx) {
	strtok(name, "\n"); // strip newline
	struct dns_rr_a4 *rr;
   u_int32_t r;
	//dns_resolve_dn(ctx, name, DNS_C_IN, DNS_T_A, 0, *dns_parse_a4);
	rr = dns_resolve_a4(ctx, name, 0);
	switch (dns_status(ctx)) {
		case 0:
			//printf("%d\n",rr->dnsa4_nrr);
			for (int j=0; j < rr->dnsa4_nrr; ++j) {
				r = ntohl(rr->dnsa4_addr[j]);
				printf("%d: %s %s ",j,rr->dnsa4_qname,rr->dnsa4_cname);
				conv_ipv4_dec_to_dotquad(r);
				printf("\n");
				//printf("%d \n",rr->dnsa4_addr[j]);
			}
			//printf("\n");
			break;
		default:
			printf("%d: %s\n",dns_status(ctx),name);
			break;
	}
	//printf("%s %s\n", prepend, name, ctx);
	return 1;
}

int conv_ipv4_dec_to_dotquad(long ip) {
// ripped from [4]
	for(int i=4;i--;)
		printf(".%hhu"+i/3,ip>>i*8);
}

#endif

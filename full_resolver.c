// File: full_resolver.c
// License: CC-BY-SA 4.0
// Author: bgstack15
// Startdate: 2020-09-02 13:59
// Title: Fully Resolve Domain Names from Stdin
// Purpose: custom output for ipv4 dns lookups
// History:
// Usage:
// References:
//    https://stackoverflow.com/a/12252195/3569534
// Improve:
// Dependencies:
//    devuan: libudns-dev
// Documentation:
//    /mnt/public/Support/Programs/dns/Troubleshooting-Adblocking-With-DNS.md
#include <stdio.h>
#include "/usr/include/udns.h"
#include "full_resolver.h"

int main(int argc, const char* argv[]) {
	char *line = NULL;
	size_t size;

	struct dns_ctx *ctx;
	dns_init(ctx,1);
	#ifdef DEBUG
	printf("BEGIN\n");
	#endif
	while (getline(&line, &size, stdin) != -1) {
		full_resolve(line, ctx);
	}
//		printf("No line\n");
   return 0;
}

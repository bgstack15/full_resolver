# File: full_resolver/Makefile
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2020-09-02
# Title: Makefile for full_resolver
# Purpose: compile the project
# History:
# Usage:
#    make DEBUG=1
# References:
#    https://bgstack15.wordpress.com/2019/11/04/sample-makefile/
# Improve:
#    investigate why the .o build section is not used
# Dependencies:
# Documentation:
#    includes nice DEBUG macro passing, which is not yet documented in reference 1 as of the document startdate.

CC = gcc
CFLAGS = -g -Wall -Wextra $(DEBUGFLAGS)

LDFLAGS = -g -ludns
ifneq (,$(DEBUG))
	LDFLAGS+=-DDEBUG=1
endif

src = $(wildcard *.c)
obj = $(src:.c=.o)

BUILD_DIR ?= .

OUTEXE = full_resolver

all: $(OUTEXE)

$(obj):

$(BUILD_DIR)/%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

# link
$(OUTEXE): $(obj)
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean cleanall list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$' -e '\.(cp*|o)'

clean:
	rm -f $(obj)

cleanall:
	rm -f $(obj) $(OUTEXE)

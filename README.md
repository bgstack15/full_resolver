# README for full_resolver
## Overview
Project `full_resolver` is part of a solution for blocking ads with dns null-routing. Specifically, this program is a troubleshooting tool for checking if a list of names are being blocked.

## Using full_resolver
Pass to standard input a list of domains, one per line. The program will resolve the domain names and display a custom output.
This project was designed primarily for taking abbreviated output of a HAR file and turning it into IP addresses.

    <input.har jq ".log.entries" | jq --raw-output "[.[] | .request.url] | sort | unique | .[]" | awk -F'/' '!x[$3]++ {print $3}' | ./full_resolver

Logic derived from [reference 3][3].

## Compiling full_resolver
Use the provided Makefile.

    make

Accepted parameters include:
1. DEBUG=1
  This will set the C macro DEBUG so the debugging fprint instructions are included.

## Dependencies
[udns](http://www.corpit.ru/mjt/udns.html), found via [https://stackoverflow.com/questions/12328093/dns-lookup-in-c-c](https://stackoverflow.com/questions/12328093/dns-lookup-in-c-c)

## References
1. [1][file:///mnt/public/Support/Programs/dns/Troubleshooting-Adblocking-with-DNS.md]  
2. [2][https://gitlab.com/bgstack15/full_resolver]
3. [3][https://gist.github.com/cvan/03ffa0c71317cb6b0b95a41ab189b097] source of jq logic
